from tracker import EuclideanDistTracker
import cv2
import numpy as np
cap  = cv2.VideoCapture('Signr.mp4')
ret, frame1 = cap.read()
ret, frame2 = cap.read()

# Get the size of the video frame
frame_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
frame_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

# Create a window with the size of the video frame
cv2.namedWindow('Video', cv2.WINDOW_NORMAL)
cv2.resizeWindow('Video', frame_width//2, frame_height//2)
frame1 = cv2.resize(frame1, (frame_width//2, frame_height//2))
frame2 = cv2.resize(frame2, (frame_width//2, frame_height//2))

tracker = EuclideanDistTracker()


while cap.isOpened():
    # ret, frame = cap.read()
    diff = cv2.absdiff(frame1, frame2)  # this method is used to find the difference bw two  frames
    gray = cv2.cvtColor(diff, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (5,5), 0 )
    # here i would add the region of interest to count the single lane cars 
    height, width = blur.shape
    # print(height, width)
    

    # thresh_value = cv2.getTrackbarPos('thresh', 'trackbar')
    _, threshold = cv2.threshold(blur, 80, 255, cv2.THRESH_BINARY)
    dilated = cv2.dilate(threshold, (1,1), iterations=1)
    contours, _, = cv2.findContours(dilated, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    detections = []
    # DRAWING RECTANGLE BOXED
    for contour in contours:
        (x,y,w,h) = cv2.boundingRect(contour)
        if cv2.contourArea(contour) <300:
            continue
        detections.append([x,y,w,h])


    # object tracking 
    boxes_ids = tracker.update(detections)
    for box_id in boxes_ids:
        x,y,w,h,id = box_id
        cv2.rectangle(frame1, (x,y),(x+w, y+h), (0,255,0), 2)
        # cv2.resizeWindow('Video', frame_width, frame_height)
        cv2.imshow('frame',frame1)


    frame1 = frame2
    ret, frame2 = cap.read()
    frame2 = cv2.resize(frame2, (frame_width//2, frame_height//2))
    
    # Wait for a key press
    key = cv2.waitKey(1)
    if key == 27:
        break
cv2.destroyAllWindows()