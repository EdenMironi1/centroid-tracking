import cv2
import numpy as np

# Load the video file
video = cv2.VideoCapture('Signr.mp4')

# Get the size of the video frame
frame_width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
frame_height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))

# Create a window with the size of the video frame
cv2.namedWindow('Video', cv2.WINDOW_NORMAL)
cv2.resizeWindow('Video', frame_width, frame_height)

# Read frames from the video file
success, frame = video.read()
i = 0

while success:
    # Convert the frame to grayscale
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    # Threshold the frame to create a binary image
    _, thresh = cv2.threshold(gray, 0,255, cv2.THRESH_BINARY)

    # Find the contours in the binary image
    contours, _ = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    # Find the contour with the largest area
    largest_contour = max(contours, key=cv2.contourArea)

    # Calculate the centroid of the contour
    moments = cv2.moments(largest_contour)
    centroid_x = int(moments['m10'] / moments['m00'])
    centroid_y = int(moments['m01'] / moments['m00'])

    # Print the centroid coordinates
    print(f'Frame {i}: Centroid = ({centroid_x}, {centroid_y})')

    # Draw the bounding box around the contour
    x, y, w, h = cv2.boundingRect(largest_contour)
    cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 0, 255), 2)

    # Draw the centroid on the frame
    cv2.circle(frame, (centroid_x, centroid_y), 5, (0, 0, 255), -1)

    # Display the frame
    cv2.imshow('Video', frame)

    # Wait for a key press
    key = cv2.waitKey(1)
    if key == 27:
        break

    # Read the next frame
    success, frame = video.read()

    # Increment the frame counter
    i += 1

# Release the video capture object and close all windows
video.release()
cv2.destroyAllWindows()

